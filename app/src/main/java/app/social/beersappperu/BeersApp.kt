package app.social.beersappperu

import android.app.Application
import app.social.beersappperu.di.app.AppComponent
import app.social.beersappperu.di.app.DaggerAppComponent
import app.social.beersappperu.di.app.NetworkModule
import app.social.beersappperu.di.app.RepositoryModule

class BeersApp:Application() {

    override fun onCreate() {
        super.onCreate()
        component()
    }

 fun component(): AppComponent =
     DaggerAppComponent.builder()
     .networkModule(NetworkModule())
     .repositoryModule(RepositoryModule())
     .build()
}