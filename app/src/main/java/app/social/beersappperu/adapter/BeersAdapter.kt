package app.social.beersappperu.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.social.beersappperu.BeersClickListener
import app.social.beersappperu.R
import app.social.beersappperu.data.model.BeersResponse
import com.squareup.picasso.Picasso

class BeersAdapter(val data: MutableList<BeersResponse>, private val listener: BeersClickListener) :
    RecyclerView.Adapter<BeersAdapter.BeersViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeersViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_beer, parent, false)
        return BeersViewHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: BeersViewHolder, position: Int) {
        val beer = data[position]

        holder.bind(beer, listener)
    }


    class BeersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val name: TextView = itemView.findViewById(R.id.tv_name)
        private val abv: TextView = itemView.findViewById(R.id.tv_abv)
        private val first_brewed: TextView = itemView.findViewById(R.id.tv_first_brewed)
        private val beerImage: ImageView = itemView.findViewById(R.id.img_beer)

        fun bind(beer: BeersResponse, listener: BeersClickListener) {
            name.text = beer.name
            abv.text = beer.abv.toString()
            first_brewed.text = beer.first_brewed.toString()

            Picasso.get().load(beer.imageUrl).into(beerImage)

            itemView.setOnClickListener {
                listener.onBeerClicked(beer)
            }

        }
    }
}