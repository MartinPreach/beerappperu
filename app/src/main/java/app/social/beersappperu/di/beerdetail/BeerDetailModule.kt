package app.social.beersappperu.di.beerdetail

import androidx.lifecycle.ViewModelProviders
import app.social.beersappperu.data.repository.Repository
import app.social.beersappperu.ui.view.BeerDetailActivity
import app.social.beersappperu.ui.viewModel.BeersViewModel
import app.social.beersappperu.ui.viewModel.factory.BeersViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class BeerDetailModule(private val beerDetailActivity: BeerDetailActivity) {

    @Provides
    @BeerDetailScope
    fun provideBeersViewModelFactory(repository: Repository): BeersViewModelFactory {
        return BeersViewModelFactory(repository)
    }

    @Provides
    @BeerDetailScope
    fun provideBeersViewModel(beersViewModelFactory: BeersViewModelFactory): BeersViewModel {

        return ViewModelProviders.of(beerDetailActivity, beersViewModelFactory)
            .get(BeersViewModel::class.java)
    }
}