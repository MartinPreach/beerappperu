package app.social.beersappperu.di.beerdetail

import app.social.beersappperu.di.app.AppComponent
import app.social.beersappperu.ui.view.BeerDetailActivity
import dagger.Component

@BeerDetailScope
@Component(modules = [BeerDetailModule::class], dependencies = [AppComponent::class])
interface BeerDetailComponent {
    fun inject(beersActivity: BeerDetailActivity)
}