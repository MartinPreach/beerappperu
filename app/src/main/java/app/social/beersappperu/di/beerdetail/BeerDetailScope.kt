package app.social.beersappperu.di.beerdetail

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class BeerDetailScope