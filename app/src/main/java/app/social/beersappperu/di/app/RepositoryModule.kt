package app.social.beersappperu.di.app

import app.social.beersappperu.data.net.BeersService
import app.social.beersappperu.data.repository.RemoteDataSource
import app.social.beersappperu.data.repository.RemoteDataSourceImpl
import app.social.beersappperu.data.repository.Repository
import app.social.beersappperu.data.repository.RepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(remoteDataSource: RemoteDataSource): Repository {
        return RepositoryImpl(remoteDataSource)
    }

    @Provides
    @Singleton
    fun provideRemoteDataSource(beersService: BeersService): RemoteDataSource {
        return RemoteDataSourceImpl(beersService)
    }
}