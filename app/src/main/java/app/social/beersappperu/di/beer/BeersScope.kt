package app.social.beersappperu.di.beer

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class BeersScope