package app.social.beersappperu.di.beer

import app.social.beersappperu.ui.view.BeersActivity
import app.social.beersappperu.di.app.AppComponent
import dagger.Component

@BeersScope
@Component(modules = [BeersModule::class],dependencies = [AppComponent::class])
interface BeersComponent {
    fun inject(beersActivity: BeersActivity)
}