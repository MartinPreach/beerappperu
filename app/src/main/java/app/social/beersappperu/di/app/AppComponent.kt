package app.social.beersappperu.di.app

import app.social.beersappperu.BeersApp
import app.social.beersappperu.data.repository.Repository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, RepositoryModule::class])
interface AppComponent {
    fun inject(myApp: BeersApp)

    fun repository(): Repository
}