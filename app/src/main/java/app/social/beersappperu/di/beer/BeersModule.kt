package app.social.beersappperu.di.beer

import androidx.lifecycle.ViewModelProviders
import app.social.beersappperu.ui.view.BeersActivity
import app.social.beersappperu.data.repository.Repository
import app.social.beersappperu.ui.viewModel.BeersViewModel
import app.social.beersappperu.ui.viewModel.factory.BeersViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class BeersModule(private val beersActivity: BeersActivity) {

    @Provides
    @BeersScope
    fun provideBeersViewModelFactory(repository: Repository): BeersViewModelFactory {
        return BeersViewModelFactory(repository)
    }

    @Provides
    @BeersScope
    fun provideBeersViewModel(beersViewModelFactory: BeersViewModelFactory): BeersViewModel {

        return ViewModelProviders.of(beersActivity, beersViewModelFactory)
            .get(BeersViewModel::class.java)
    }
}