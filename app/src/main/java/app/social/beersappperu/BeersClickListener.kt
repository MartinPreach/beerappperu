package app.social.beersappperu

import app.social.beersappperu.data.model.BeersResponse

interface BeersClickListener {
    fun onBeerClicked(beer: BeersResponse)
}