package app.social.beersappperu.ui.viewModel

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.social.beersappperu.data.model.BeersResponse
import app.social.beersappperu.ui.view.BeerDetailActivity
import javax.inject.Inject

class BeerDetailViewModel @Inject constructor() : ViewModel() {

    private val beerObservable: MutableLiveData<BeersResponse> = MutableLiveData()
    private val errorObservable: MutableLiveData<Boolean> = MutableLiveData()


    fun getBeerDetail(intent: Intent) {
        if (intent.extras?.containsKey(BeerDetailActivity.BEER_KEY) == true) {
            errorObservable.value = false
            beerObservable.value = intent.getParcelableExtra(BeerDetailActivity.BEER_KEY)

        } else {
            errorObservable.value = true
        }
    }

    fun getBeersObservable() = beerObservable
    fun getErrorObservable() = errorObservable

}