package app.social.beersappperu.ui.view

import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.lifecycle.Observer
import app.social.beersappperu.BeersApp
import app.social.beersappperu.R
import app.social.beersappperu.di.beerdetail.BeerDetailModule
import app.social.beersappperu.data.model.BeersResponse
import app.social.beersappperu.ui.viewModel.BeerDetailViewModel
import app.social.beersappperu.di.beerdetail.DaggerBeerDetailComponent
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_beer_detail.*
import javax.inject.Inject


class BeerDetailActivity : AppCompatActivity() {

    lateinit var beer: BeersResponse

    private lateinit var scaleAnimation: Animation

    private lateinit var cv_detail: CardView

    @Inject
    lateinit var beerDetailViewModel: BeerDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_beer_detail)
        getDependencies()
        onBackPressedDispatcher

        scaleAnimation = AnimationUtils.loadAnimation(this, R.anim.scale)

        cv_detail = findViewById(R.id.cv_detail)

        cv_detail.animation = scaleAnimation

        beerDetailViewModel.getBeersObservable().observe(this, Observer {

            tv_name.text = it.name

            Picasso.get().load(it.imageUrl).into(iv_photo)

            tv_description.text = it.description
            tv_hops.text =
                it.ingredients.hops.joinToString(separator = ",", transform = { hop ->
                    hop.name
                })
            tv_malt.text =
                it.ingredients.malt.joinToString(separator = ",", transform = { malt ->
                    malt.name
                })
            tv_food_pairings.text = it.foodPairing.joinToString(",")
        })
        beerDetailViewModel.getBeerDetail(intent)
    }

    private fun getDependencies() {
        DaggerBeerDetailComponent.builder().appComponent((application as BeersApp).component())
            .beerDetailModule(
                BeerDetailModule(this)
            ).build().inject(this)

    }

    companion object {
        const val BEER_KEY = "beer"
    }
}