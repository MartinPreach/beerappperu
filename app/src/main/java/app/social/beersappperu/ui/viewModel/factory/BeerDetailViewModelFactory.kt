package app.social.beersappperu.ui.viewModel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.social.beersappperu.data.repository.Repository
import app.social.beersappperu.ui.viewModel.BeersViewModel

class BeerDetailViewModelFactory(private val repository: Repository):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BeersViewModel(repository) as T
    }
}