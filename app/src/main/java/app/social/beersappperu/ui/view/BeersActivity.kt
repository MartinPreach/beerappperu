package app.social.beersappperu.ui.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import app.social.beersappperu.BeersClickListener
import app.social.beersappperu.BeersApp
import app.social.beersappperu.R
import app.social.beersappperu.adapter.BeersAdapter
import app.social.beersappperu.di.beer.BeersModule
import app.social.beersappperu.di.beer.DaggerBeersComponent
import app.social.beersappperu.data.model.BeersResponse
import app.social.beersappperu.ui.viewModel.BeersViewModel
import kotlinx.android.synthetic.main.activity_beers.*
import javax.inject.Inject

class BeersActivity : AppCompatActivity() {

    private lateinit var adapter: BeersAdapter
    private lateinit var Sv: SearchView

    @Inject
    lateinit var beersViewModel: BeersViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beers)

        Sv = findViewById(R.id.Sv)

        setUpRecyclerView()
        getDependencies()

        beersViewModel.getBeers()

        beersViewModel.getBeersObservable().observe(this, Observer {
            adapter.data.clear()
            adapter.data.addAll(it)
            adapter.notifyDataSetChanged()
        })

        beersViewModel.getProgressObservable().observe(this, Observer {
            if (it == true) {
                pb_loading.visibility = View.VISIBLE
            } else {
                pb_loading.visibility = View.GONE
            }
        })

        beersViewModel.getErrorObservable().observe(this, Observer {
            if (it == true) {
                error_message_container.visibility = View.VISIBLE
            } else {
                error_message_container.visibility = View.GONE
            }
        })

        btnRetry.setOnClickListener {
            beersViewModel.getBeers()
            error_message_container.visibility = View.GONE

        }

        Sv.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                //Performs search when user hit the search button on the keyboard
//                if (tv_name.Constants(p0)) {
//                    adapter.filter.filter(p0)
//                } else {
//                    Toast.makeText(this@BeersActivity, "No match found", Toast.LENGTH_SHORT).show()
//                }
                return false
            }
            override fun onQueryTextChange(p0: String?): Boolean {
                //Start filtering the list as user start entering the characters
                //adapter.filter.filter(p0)
                return false
            }
        })
    }

    private fun setUpRecyclerView() {
        adapter = BeersAdapter(mutableListOf(), object : BeersClickListener {
            override fun onBeerClicked(beer: BeersResponse) {
                val intent = Intent(this@BeersActivity, BeerDetailActivity::class.java)
                intent.putExtra(BEER_KEY, beer)
                startActivity(intent)
            }

        })

        rv_beer_list.layoutManager = LinearLayoutManager(this)
        rv_beer_list.adapter = adapter
    }

    private fun getDependencies() {
        DaggerBeersComponent.builder().appComponent((application as BeersApp).component()).beersModule(
            BeersModule(this)
        ).build().inject(this)

    }

    companion object {
        const val BEER_KEY = "beer"
    }
}

