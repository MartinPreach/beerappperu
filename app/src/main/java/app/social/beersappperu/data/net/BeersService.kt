package app.social.beersappperu.data.net

import app.social.beersappperu.common.BEER_ENDPOINT
import app.social.beersappperu.data.model.BeersResponse
import io.reactivex.Single
import retrofit2.http.GET

interface BeersService{
    @GET(BEER_ENDPOINT)
    fun getBeers(): Single<List<BeersResponse>>
}

