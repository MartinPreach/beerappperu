package app.social.beersappperu.data.repository

import app.social.beersappperu.data.model.BeersResponse
import io.reactivex.Single

interface Repository {
    fun getBeers():Single<List<BeersResponse>>
}