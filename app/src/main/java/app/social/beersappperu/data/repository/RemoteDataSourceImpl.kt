package app.social.beersappperu.data.repository

import app.social.beersappperu.data.model.BeersResponse
import app.social.beersappperu.data.net.BeersService
import io.reactivex.Single

import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(private val beersService: BeersService) :
    RemoteDataSource {
    override fun getBeers(): Single<List<BeersResponse>> {
        return beersService.getBeers()
    }


}